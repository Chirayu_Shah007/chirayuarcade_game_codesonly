﻿using Photon.Pun;
using System.IO;
using UnityEngine;

public class GameSetupController : MonoBehaviour
{
    public static GameSetupController gameSetupController;

    public Transform[] spawnPoints;

    // Singleton 
    private void OnEnable()
    {
        if(GameSetupController.gameSetupController == null)
        {
            GameSetupController.gameSetupController = this;
        }
    }
    
    private void Start()
    {
        CreatePlayer();
    }

    private void CreatePlayer()
    {
        Debug.Log("Creating Player");
        PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PhotonPlayer"), Vector3.zero, Quaternion.identity);
    }
    
}
