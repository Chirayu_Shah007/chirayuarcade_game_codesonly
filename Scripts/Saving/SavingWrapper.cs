﻿using UnityEngine;
using CJArcade.Saving;
using System.Collections;
using UnityEngine.SceneManagement;

namespace CJArcade.SceneManagement
{
    public class SavingWrapper : MonoBehaviour
    {
        [SerializeField] float fadeInTime = 0.5f;
        const string defaultSaveFile = "save";

        
        private bool saveGame = false;
        private bool loadGame = false;
        private bool deleteSave = false;

        private void Awake()
        {
            Debug.Log("Hello!");
            //StartCoroutine(LoadLastScene());
        }

        private IEnumerator LoadLastScene()
        {
            //Fader fader = FindObjectOfType<Fader>();

            yield return GetComponent<SavingSystem>().LoadLastScene(defaultSaveFile);
            //fader.FadeOutImmediate();
           //yield return fader.FadeIn(fadeInTime);

        }
        // Update is called once per frame
        void Update()
        {

            if (Input.GetKeyDown(KeyCode.F2))
            {
                Load();
                print("Loaded");
            }
            if (Input.GetKeyDown(KeyCode.F1))
            {
                Save();
                print("Saved");
            }
            if (Input.GetKeyDown(KeyCode.F3))
            {
                Delete();
            }

        }

        public void Save()
        {
            GetComponent<SavingSystem>().Save(defaultSaveFile);
        }

        public void Load()
        {
            GetComponent<SavingSystem>().Load(defaultSaveFile);
        }

        public void Delete()
        {
            GetComponent<SavingSystem>().Delete(defaultSaveFile);
        }

        public void OnClick()
        {
            //if (other.gameObject.tag == "Player")
            {
                StartCoroutine(Transition());
            }
        }

        public void OnEndEdit(string name)
        {
            Debug.Log("Name is: " + name);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Player")
            {
                StartCoroutine(Transition());
            }
        }

        private IEnumerator Transition()
        {
            if (1 < 0)
            {
                Debug.LogError("Scene to load not set.");
                yield break;
            }

            //Fader fader = FindObjectOfType<Fader>();
            DontDestroyOnLoad(gameObject);

            //yield return fader.FadeOut(100);

            SavingWrapper wrapper = FindObjectOfType<SavingWrapper>();
            wrapper.Save();

            //yield return SceneManager.LoadSceneAsync(1);

            wrapper.Load();

            //Portal otherPortal = GetOtherPortal();
            //UpdatePlayer(otherPortal);

            wrapper.Save();

            //yield return new WaitForSeconds(100);
            //yield return fader.FadeIn(fadeInTime);

            Destroy(gameObject);
        }

    }
}