﻿using System.Collections;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;
using TMPro;
using CJArcade.SceneManagement;
using UnityEngine.SceneManagement;

public class CustomMatchmakingRoomController : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private int multiPlayerSceneIndex; // scene index for loading multiplayer scene

    [SerializeField]
    private GameObject lobbyPanel; // display for when in lobby
    [SerializeField]
    private GameObject roomPanel; // display for when in room

    [SerializeField]
    private GameObject startButton; // only for the master client to start the game

    [SerializeField]
    private Transform playersContainer; // used to display all the players in current room
    [SerializeField]
    private GameObject playerListingPrefab; // Instantiate to display each player in the room

    [SerializeField]
    private TMP_Text roomNameDisplay; // display for the name of the room

    void ClearPlayerListings()
    {
        for (int i = playersContainer.childCount - 1; i >= 0; i--) // loop through all child objects
        {
            Destroy(playersContainer.GetChild(i).gameObject);
        }
    }
    void ListPlayers()
    {
        foreach(Player player in PhotonNetwork.PlayerList)
        {
            GameObject tempListing = Instantiate(playerListingPrefab, playersContainer);
            TMP_Text tempText = tempListing.transform.GetChild(0).GetComponent<TMP_Text>();
            tempText.text = player.NickName;
        }
    }

    public override void OnJoinedRoom() // called when the local player jooins the room
    {
        roomPanel.SetActive(true);
        lobbyPanel.SetActive(false);
        roomNameDisplay.text = PhotonNetwork.CurrentRoom.Name; // update room name display
        if(PhotonNetwork.IsMasterClient)
        {
            startButton.SetActive(true);
        }
        else
        {
            startButton.SetActive(false);
        }
        ClearPlayerListings(); //remove all old player listings
        ListPlayers(); // relist all current player listings
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        ClearPlayerListings();
        ListPlayers();
    }
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        ClearPlayerListings();
        ListPlayers();
        if(PhotonNetwork.IsMasterClient) // if the local player is now the new master client
        {
            startButton.SetActive(true);
        }
    }

    public void StartGame() // linked with start button. This will load all player into multiplayer server
    {
        if(PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.CurrentRoom.IsOpen = false; // Comment out if you want player to join after game is started
            
            PhotonNetwork.LoadLevel(multiPlayerSceneIndex);
          

            
            //Load the saved player
            SavingWrapper wrapper = FindObjectOfType<SavingWrapper>();
            wrapper.Load();
            
        }
    }

    IEnumerator rejoinLobby()
    {
        yield return new WaitForSeconds(1);
        PhotonNetwork.JoinLobby();
    }

    public void BackOnClick()
    {
        lobbyPanel.SetActive(true);
        roomPanel.SetActive(false);
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.LeaveLobby();
        StartCoroutine(rejoinLobby());
    }
}
