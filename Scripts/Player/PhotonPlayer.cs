﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Photon.Pun;

public class PhotonPlayer : MonoBehaviour
{
    private PhotonView PV;
    public GameObject myAvatar;
    void Start()
    {
        PV = GetComponent<PhotonView>();
        int spawnPicker = Random.Range(0, GameSetupController.gameSetupController.spawnPoints.Length);
        if(PV.IsMine)
        {
            myAvatar = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PhotonPlayer"),
                        GameSetupController.gameSetupController.spawnPoints[spawnPicker].position, GameSetupController.gameSetupController.spawnPoints[spawnPicker].rotation, 0);
        }
    }

    
    void Update()
    {
        
    }
}
